﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Common
{
	public class CommonDef
	{
		// 格闘→魔法
		// 射撃→格闘
		// 魔法→射撃
		
		// バトル関連
		// 属性
		public enum Attr
		{
			Atk = 0, // 格闘
			Shot,    // 射撃
			Magic,   // 魔法
		}
		
		// 属性
		public enum AttrJudge
		{
			Win,
			Lose,
			Draw,
		}
		
		public const int ROUND_MAX = 3;

		public static readonly Dictionary<Attr, string> ATTR_ICON_IMAGE_NAME = new Dictionary<Attr, string>(){
			{ Attr.Atk, "Icon_atk" },
			{ Attr.Shot, "Icon_shot" },
			{ Attr.Magic, "Icon_magic" },
		};
	}
}
