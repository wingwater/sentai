﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;

namespace Battle{
	/// <summary>
	/// バトル情報UI
	/// </summary>
	public class BattleInfoUI : MonoBehaviour
	{
		/// <summary> </summary>
		[SerializeField] private GameObject _goAttrInfo;
		[SerializeField] private Text _textNavi;

		/// <summary>
		/// Start
		/// </summary>
		void Start()
		{
		}

		/// <summary>
		/// Init
		/// </summary>
		public void Init()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public void Player1TurnStart()
		{
			_goAttrInfo.SetActive(true);
			this.SetTextNavi("←Player1 Command");
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void Player2TurnStart()
		{
			_goAttrInfo.SetActive(true);
			this.SetTextNavi("Player2 Command→");
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void BattleStart()
		{
			_goAttrInfo.SetActive(true);
			this.SetTextNavi("");
		}
		

		/// <summary>
		/// ナビテキスト設定
		/// </summary>
		private void SetTextNavi(string in_text)
		{
			_textNavi.text = in_text;
		}

		/// <summary>
		/// 破棄
		/// </summary>
		void OnDestroy()
		{
		}
	}
}
