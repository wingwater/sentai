﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;

namespace Battle{
	/// <summary>
	/// バトルシーンマネージャー
	/// </summary>
	public class BattleSceneManager : MonoBehaviour
	{
		private enum STATE{
			Player1Turn,
			Player2Turn,
			Battle,
		}
		
		/// <summary> プレーヤー </summary>
		[SerializeField] private Player _player1;
		[SerializeField] private Player _player2;

		[SerializeField] private BattleInfoUI _infoUI;

		[SerializeField] private BattleDirection _battleDirection;

		private STATE _state = STATE.Player1Turn;

		/// <summary>
		/// Start
		/// </summary>
		void Start()
		{
			this.Init();
		}

		/// <summary>
		/// Init
		/// </summary>
		public void Init()
		{
			_player1.Init();
			_player2.Init();

			this.InitPlayer1Turn();
		}

		/// <summary>
		/// 更新
		/// </summary>
		void Update()
		{
			// ※※色々ステートマシン整理する
			
			STATE afterState = _state;
			
			switch(_state)
			{
			case STATE.Player1Turn:
				afterState = this.UpdatePlayer1Turn();
				break;
			case STATE.Player2Turn:
				afterState = this.UpdatePlayer2Turn();
				break;
			case STATE.Battle:
				afterState = this.UpdateBattle();
				break;
			}

			if(_state != afterState)
			{
				switch(_state)
				{
				case STATE.Player1Turn:
					this.ExitPlayer1Turn();
					break;
				case STATE.Player2Turn:
					this.ExitPlayer2Turn();
					break;
				case STATE.Battle:
					this.ExitBattle();
					break;
				}

				_state = afterState;
				
				switch(_state)
				{
				case STATE.Player1Turn:
					this.InitPlayer1Turn();
					break;
				case STATE.Player2Turn:
					this.InitPlayer2Turn();
					break;
				case STATE.Battle:
					this.InitBattle();
					break;
				}
			}
		}
		
		/// <summary>
		/// Player1Turn 初期化
		/// </summary>
		private void InitPlayer1Turn()
		{
			_player1.ActionTurnStart();
			_player2.AtcionEnemyTurnStart();
			_infoUI.Player1TurnStart();
		}
		/// <summary>
		/// Player1Turn 更新
		/// </summary>
		private STATE UpdatePlayer1Turn()
		{
			if(_player1.IsCommandEnd())
			{
				return STATE.Player2Turn;
			}
			else
			{
				return STATE.Player1Turn;
			}
		}
		/// <summary>
		/// Player1Turn 終了
		/// </summary>
		private void ExitPlayer1Turn()
		{
			_player1.ActionTurnEnd();
			_player2.AtcionEnemyTurnEnd();
		}

		/// <summary>
		/// Player2Turn 初期化
		/// </summary>
		private void InitPlayer2Turn()
		{
			_player1.AtcionEnemyTurnStart();
			_player2.ActionTurnStart();
			_infoUI.Player2TurnStart();
		}
		/// <summary>
		/// Player2Turn 更新
		/// </summary>
		private STATE UpdatePlayer2Turn()
		{
			if(_player2.IsCommandEnd())
			{
				return STATE.Battle;
			}
			else
			{
				return STATE.Player2Turn;
			}
		}
		/// <summary>
		/// Player2Turn 終了
		/// </summary>
		private void ExitPlayer2Turn()
		{
			_player1.AtcionEnemyTurnEnd();
			_player2.ActionTurnEnd();
		}

		/// <summary>
		/// Battle 初期化
		/// </summary>
		private void InitBattle()
		{
			_battleDirection.Play(_player1.NowCommandList, _player2.NowCommandList);
			_infoUI.BattleStart();
		}
		/// <summary>
		/// Battle 更新
		/// </summary>
		private STATE UpdateBattle()
		{
			return _battleDirection.IsDirectionEnd() ? STATE.Player1Turn : STATE.Battle;
		}
		/// <summary>
		/// Battle 終了
		/// </summary>
		private void ExitBattle()
		{
			_player1.AtcionBattleEnd();
			_player2.AtcionBattleEnd();
			_battleDirection.End();
		}

		
		/// <summary>
		/// 破棄
		/// </summary>
		void OnDestroy()
		{
		}
	}
}
