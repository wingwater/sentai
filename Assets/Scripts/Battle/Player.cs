﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;

namespace Battle{
	/// <summary>
	/// プレーヤー
	/// </summary>
	public class Player : MonoBehaviour
	{
		/// <summary>  </summary>
		[SerializeField] private GameObject _goCommand;
		[SerializeField] private GameObject _goHistory;
		[SerializeField] private Button _buttonCommandAtk;
		[SerializeField] private Button _buttonCommandShot;
		[SerializeField] private Button _buttonCommandMagic;
		[SerializeField] private Button _buttonDecide;
		[SerializeField] private Button _buttonCancel;

		[SerializeField] private List<Image> _imageHistoryList;
		[SerializeField] private Text _textHistoryNavi;

		private List<CommonDef.Attr> _nowCommandList = new List<CommonDef.Attr>();
		public List<CommonDef.Attr> NowCommandList{ get{ return _nowCommandList; } }
		private List<CommonDef.Attr> _prevCommandList = new List<CommonDef.Attr>();
		public List<CommonDef.Attr> prevCommandList{ get{ return _prevCommandList; } }
		private bool _isCommandEnd = false;
		private int _hpNow;
		private int _hpMax;

		/// <summary>
		/// Start
		/// </summary>
		void Start()
		{
		}

		/// <summary>
		/// Init
		/// </summary>
		public void Init()
		{
			_hpNow = _hpMax = 1000;
			SetHPNow(_hpNow);

			// 初めは非アクティブ
			_goCommand.SetActive(false);
			_goHistory.SetActive(false);
		}

		/// <summary>
		/// アクション：TurnStart
		/// </summary>
		public void ActionTurnStart()
		{
			_goCommand.SetActive(true);
			_goHistory.SetActive(true);

			_nowCommandList.Clear();
			_isCommandEnd = false;

			this.UpdateCommandDisp();
		}

		/// <summary>
		/// アクション：TurnEnd
		/// </summary>
		public void ActionTurnEnd()
		{
			_goCommand.SetActive(false);
			_goHistory.SetActive(false);
		}

		/// <summary>
		/// アクション：AtcionEnemyTurnStart
		/// </summary>
		public void AtcionEnemyTurnStart()
		{
			this.DispCommandHistory();
		}
		
		/// <summary>
		/// アクション：AtcionEnemyTurnEnd
		/// </summary>
		public void AtcionEnemyTurnEnd()
		{
			_goCommand.SetActive(false);
			_goHistory.SetActive(false);
		}
		
		/// <summary>
		/// アクション：AtcionBattleEnd
		/// </summary>
		public void AtcionBattleEnd()
		{
			_prevCommandList = new List<CommonDef.Attr>(_nowCommandList);
		}
		
		/// <summary>
		/// コマンド入力済み？
		/// </summary>
		public bool IsCommandEnd()
		{
			return _isCommandEnd;
		}
		
		/// <summary>
		/// 現在 HP セット
		/// </summary>
		public void SetHPNow(int in_hp)
		{
			_hpNow = Mathf.Min(in_hp, _hpMax);
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void OnSelectAtk()
		{
			_nowCommandList.Add(CommonDef.Attr.Atk);
			this.UpdateCommandDisp();
		}
		public void OnSelectShot()
		{
			_nowCommandList.Add(CommonDef.Attr.Shot);
			this.UpdateCommandDisp();
		}
		public void OnSelectMagic()
		{
			_nowCommandList.Add(CommonDef.Attr.Magic);
			this.UpdateCommandDisp();
		}

		/// <summary>
		/// 決定
		/// </summary>
		public void OnDecide()
		{
			_isCommandEnd = true;
		}
		
		/// <summary>
		/// 属性選択キャンセル
		/// </summary>
		public void OnCancelAttr()
		{
			if(_nowCommandList.Count > 0)
			{
				// ※※ユーザビリティ考えるとメンバー行動を保持しとく
				
				_nowCommandList.RemoveAt(_nowCommandList.Count-1);

				//
				this.UpdateCommandDisp();
			}
		}

		/// <summary>
		/// 破棄
		/// </summary>
		void OnDestroy()
		{
		}
		
		/// <summary>
		/// コマンド系見た目更新
		/// </summary>
		private void UpdateCommandDisp()
		{
			var round = this.GetRound();
			var isFirstRound = round == 0;
			
			// 各属性コマンドボタン
			_buttonCommandAtk.interactable = this.IsSelectableAttr(CommonDef.Attr.Atk, isFirstRound);
			_buttonCommandShot.interactable = this.IsSelectableAttr(CommonDef.Attr.Shot, isFirstRound);
			_buttonCommandMagic.interactable = this.IsSelectableAttr(CommonDef.Attr.Magic, isFirstRound);

			// 履歴
			for(var i=0; i<CommonDef.ROUND_MAX; i++)
			{
				if(i < _nowCommandList.Count)
				{
					_imageHistoryList[i].gameObject.SetActive(true);

					string path = "Texture/"+CommonDef.ATTR_ICON_IMAGE_NAME[_nowCommandList[i]];
					Texture2D texture = Resources.Load(path) as Texture2D;
					_imageHistoryList[i].sprite = Sprite.Create(texture,
																new Rect(0, 0, texture.width, texture.height),
																new Vector2(0.5f, 0.5f));
				}
				else
				{
					_imageHistoryList[i].gameObject.SetActive(false);
				}
			}
			_textHistoryNavi.gameObject.SetActive(false);

			// 決定ボタン
			_buttonDecide.interactable = (round == CommonDef.ROUND_MAX);

			// キャンセルボタン
			_buttonCancel.interactable = !isFirstRound;
		}
		
		/// <summary>
		/// 前のターンの履歴表示
		/// </summary>
		private void DispCommandHistory()
		{
			for(var i=0; i<CommonDef.ROUND_MAX; i++)
			{
				if(i < _prevCommandList.Count)
				{
					_imageHistoryList[i].gameObject.SetActive(true);

					string path = "Texture/"+CommonDef.ATTR_ICON_IMAGE_NAME[_prevCommandList[i]];
					Texture2D texture = Resources.Load(path) as Texture2D;
					_imageHistoryList[i].sprite = Sprite.Create(texture,
																new Rect(0, 0, texture.width, texture.height),
																new Vector2(0.5f, 0.5f));
				}
				else
				{
					_imageHistoryList[i].gameObject.SetActive(false);
				}
			}
			_goHistory.SetActive(true);
			_textHistoryNavi.gameObject.SetActive(_prevCommandList.Count > 0);
		}
		
		/// <summary>
		/// 現在選択中のラウンド取得
		/// </summary>
		private int GetRound()
		{
			return _nowCommandList.Count;
		}
		
		/// <summary>
		/// 指定属性は選択可能？
		/// </summary>
		/// <param name="in_attr">属性</param>
		/// <param name="in_isFirstRound">第一ラウンド？</param>
		private bool IsSelectableAttr(CommonDef.Attr in_attr, bool in_isFirstRound)
		{
			List<CommonDef.Attr> list = in_isFirstRound ? _prevCommandList : _nowCommandList;
			return (list.Count == 0 || (list[list.Count-1] != in_attr && _nowCommandList.Count < CommonDef.ROUND_MAX));
		}
	}
}
