﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;

namespace Battle{

	/// <summary>
	/// バトル
	/// </summary>
	public class BattleUtil
	{
		public static CommonDef.AttrJudge JudgeAttr(CommonDef.Attr in_attrPlayer, CommonDef.Attr in_attrEnemy)
		{
			CommonDef.AttrJudge judge = CommonDef.AttrJudge.Draw;
			
			switch(in_attrPlayer)
			{
			case CommonDef.Attr.Atk:
				switch(in_attrEnemy)
				{
				case CommonDef.Attr.Atk:   judge = CommonDef.AttrJudge.Draw; break;
				case CommonDef.Attr.Shot:  judge = CommonDef.AttrJudge.Lose; break;
				case CommonDef.Attr.Magic: judge = CommonDef.AttrJudge.Win; break;
				}
				break;
				
			case CommonDef.Attr.Shot:
				switch(in_attrEnemy)
				{
				case CommonDef.Attr.Atk:   judge = CommonDef.AttrJudge.Win; break;
				case CommonDef.Attr.Shot:  judge = CommonDef.AttrJudge.Draw; break;
				case CommonDef.Attr.Magic: judge = CommonDef.AttrJudge.Lose; break;
				}
				break;
				
			case CommonDef.Attr.Magic:
				switch(in_attrEnemy)
				{
				case CommonDef.Attr.Atk:   judge = CommonDef.AttrJudge.Lose; break;
				case CommonDef.Attr.Shot:  judge = CommonDef.AttrJudge.Win; break;
				case CommonDef.Attr.Magic: judge = CommonDef.AttrJudge.Draw; break;
				}
				break;
			}

			return judge;
		}
	}
}
