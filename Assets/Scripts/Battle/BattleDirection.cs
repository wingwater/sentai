﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;

namespace Battle{
	/// <summary>
	/// バトル演出
	/// </summary>
	public class BattleDirection : MonoBehaviour
	{
		/// <summary> </summary>
		[SerializeField] private GameObject _goPlayer1;
		[SerializeField] private GameObject _goPlayer2;
		[SerializeField] private List<Image> _imagePlayer1CommandList;
		[SerializeField] private List<Image> _imagePlayer2CommandList;
		[SerializeField] private Button _buttonEnd;

		private bool _isEnd = false;

		/// <summary>
		/// Start
		/// </summary>
		void Start()
		{
			_goPlayer1.SetActive(false);
			_goPlayer2.SetActive(false);
			_buttonEnd.gameObject.SetActive(false);
		}

		/// <summary>
		/// Init
		/// </summary>
		public void Init()
		{
		}

		/// <summary>
		/// Play
		/// </summary>
		public void Play(List<CommonDef.Attr> in_player1Command, List<CommonDef.Attr> in_player2Command)
		{
			if(in_player1Command.Count != in_player2Command.Count) return;
			
			_goPlayer1.SetActive(true);
			_goPlayer2.SetActive(true);
			_buttonEnd.gameObject.SetActive(true);
			_isEnd = false;

			// Player1
			for(var i=0; i<in_player1Command.Count; i++)
			{
				if(i < _imagePlayer1CommandList.Count)
				{
					_imagePlayer1CommandList[i].gameObject.SetActive(true);

					string path = "Texture/"+CommonDef.ATTR_ICON_IMAGE_NAME[in_player1Command[i]];
					Texture2D texture = Resources.Load(path) as Texture2D;
					_imagePlayer1CommandList[i].sprite = Sprite.Create(texture,
																	   new Rect(0, 0, texture.width, texture.height),
																	   new Vector2(0.5f, 0.5f));
				}
				else
				{
					_imagePlayer1CommandList[i].gameObject.SetActive(false);
				}
			}
			
			// Player2
			for(var i=0; i<in_player2Command.Count; i++)
			{
				if(i < _imagePlayer2CommandList.Count)
				{
					_imagePlayer2CommandList[i].gameObject.SetActive(true);

					string path = "Texture/"+CommonDef.ATTR_ICON_IMAGE_NAME[in_player2Command[i]];
					Texture2D texture = Resources.Load(path) as Texture2D;
					_imagePlayer2CommandList[i].sprite = Sprite.Create(texture,
																	   new Rect(0, 0, texture.width, texture.height),
																	   new Vector2(0.5f, 0.5f));
				}
				else
				{
					_imagePlayer2CommandList[i].gameObject.SetActive(false);
				}
			}

			// 優劣可視化
			for(var i=0; i<in_player1Command.Count; i++)
			{
				switch(BattleUtil.JudgeAttr(in_player1Command[i], in_player2Command[i]))
				{
				case CommonDef.AttrJudge.Win:
					_imagePlayer1CommandList[i].color = Color.white;
					_imagePlayer2CommandList[i].color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
					break;
				case CommonDef.AttrJudge.Draw:
					_imagePlayer1CommandList[i].color = Color.white;
					_imagePlayer2CommandList[i].color = Color.white;
					break;
				case CommonDef.AttrJudge.Lose:
					_imagePlayer1CommandList[i].color = new Color(0.25f, 0.25f, 0.25f, 1.0f);
					_imagePlayer2CommandList[i].color = Color.white;
					break;
				}
			}
		}

		/// <summary>
		/// End
		/// </summary>
		public void End()
		{
			_goPlayer1.SetActive(false);
			_goPlayer2.SetActive(false);
			_buttonEnd.gameObject.SetActive(false);
		}
		
		/// <summary>
		/// Update
		/// </summary>
		void Update()
		{
		}

		/// <summary>
		/// 決定
		/// </summary>
		public void OnEnd()
		{
			_isEnd = true;
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsDirectionEnd()
		{
			return _isEnd;
		}

		/// <summary>
		/// 破棄
		/// </summary>
		void OnDestroy()
		{
		}
	}
}
